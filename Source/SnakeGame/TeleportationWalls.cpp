// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportationWalls.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ATeleportationWalls::ATeleportationWalls()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	WallRight = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallRight"));
	WallRight->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	WallRight->SetCollisionResponseToAllChannels(ECR_Overlap);
	WallLeft = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallLeft"));
	WallLeft->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	WallLeft->SetCollisionResponseToAllChannels(ECR_Overlap);
	WallUp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallUp"));
	WallUp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	WallUp->SetCollisionResponseToAllChannels(ECR_Overlap);
	WallDown = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WallDown"));
	WallDown->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	WallDown->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void ATeleportationWalls::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleportationWalls::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleportationWalls::Interact(AActor* Interact, bool bIsHead, UPrimitiveComponent* OtherComp)
{
	float X, Y, Z;
	//
	auto SnakeActor = Cast<ASnakeBase>(Interact);
	/*if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, OtherComp->GetName());*/
	
	if (IsValid(SnakeActor))
	{
		
		ASnakeElementBase* Firstlement = SnakeActor->SnakeElements[0];
		Z = Interact->GetActorLocation().Z;
		if (WallRight->GetName() == OtherComp->GetName())
		{
			X = Firstlement->GetActorLocation().X;
			Y = WallLeft->GetComponentLocation().Y + 150;	
			
		}
		if (WallLeft->GetName() == OtherComp->GetName())
		{
			X = Firstlement->GetActorLocation().X;
			Y = WallRight->GetComponentLocation().Y - 150;
		}
		if (WallUp->GetName() == OtherComp->GetName())
		{
			X = WallDown->GetComponentLocation().X + 150;
			Y = Firstlement->GetActorLocation().Y;
		}
		if (WallDown->GetName() == OtherComp->GetName())
		{
			X = WallUp->GetComponentLocation().X - 150;
			Y = Firstlement->GetActorLocation().Y;
			
		}
		Firstlement->SetActorLocation(FVector(X, Y, Z));
	}
	
}


