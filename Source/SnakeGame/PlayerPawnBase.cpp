// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SpawnGrid.h"
#include "Math/TransformNonVectorized.h"
#include "Components/InputComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));	
	RootComponent = PawnCamera;
	
}

void APlayerPawnBase::HandlerPlayerDebugButton()
{
			

	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Debug"));

}

void APlayerPawnBase::HandlerPlayerVerticalInput(float Value)
{
	
	if (IsValid(SnakeActor))
	{
		if (Value > 0 && SnakeActor->GetLastMovementDirection() != EMoveDirection::DOWN)
		{
			SnakeActor->SetLastMovementDirection(EMoveDirection::UP);			
		}
		if (Value < 0 && SnakeActor->GetLastMovementDirection() != EMoveDirection::UP)
		{
			SnakeActor->SetLastMovementDirection(EMoveDirection::DOWN);
		}
	}
}

void APlayerPawnBase::HandlerPlayerHorizontalInput(float Value)
{
	
	if (IsValid(SnakeActor))
	{
		if (Value > 0 && SnakeActor->GetLastMovementDirection() != EMoveDirection::LEFT)
		{
			SnakeActor->SetLastMovementDirection(EMoveDirection::RIGHT);
		}
		if (Value < 0 && SnakeActor->GetLastMovementDirection() != EMoveDirection::RIGHT)
		{
			SnakeActor->SetLastMovementDirection(EMoveDirection::LEFT);
		}
	}
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlerPlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlerPlayerHorizontalInput);
	PlayerInputComponent->BindAction("DebuggButton", IE_Pressed, this, &APlayerPawnBase::HandlerPlayerDebugButton);
}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	ActorSpawn = GetWorld()->SpawnActor<ASpawnGrid>(ASpawnGridClass, FTransform(FRotator(0, 90, 0),FVector(0, 0, -60)));
}

