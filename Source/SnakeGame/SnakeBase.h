// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TimerManager.h"
#include "SnakeBase.generated.h"

UENUM()
enum class EMoveDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

class ASnakeElementBase;
UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;
	void SetLastMovementDirection(EMoveDirection LastMoveDirection);
	EMoveDirection GetLastMovementDirection();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void AddSnakeElements(int EmlementNum = 1);
	void Move();
	void AddLife();
	void SpeedIncrease(float SpeedIncriment);
	int GetLifeCount();
	void ReduceLifeCount();
	void SetToInvulnerable();
	bool GetStepDone();
	bool GetInvulnerable();
	void DestroyAllElements();

	
	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedBlock, AActor* OtherActor, UPrimitiveComponent* OtherComp);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;
	bool IsInvulnerable = false;

private:
	bool StepDone;	
	EMoveDirection LastMoveDirection;
	int LifeCount = 1;
	FTimerHandle MemberTimerHandle;
	void InvulnerabilityTimer();
};
