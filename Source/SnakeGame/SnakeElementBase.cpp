// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "TeleportationWalls.h"


// Sets default values
ASnakeElementBase::ASnakeElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

void ASnakeElementBase::SetFirstElementType_Implementation()
{
	IsUnbreakble = true;
	MeshComponent->SetMaterial(0, LifeMaterial);
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeElementBase::HandleBeginOverlap);
}


void ASnakeElementBase::DestroyElement()
{
	this->Destroy();
}

void ASnakeElementBase::ChangeMaterial()
{
	if (MeshComponent->GetMaterial(0) == DefaultMaterial)
		MeshComponent->SetMaterial(0, GhostMaterial);
	if (MeshComponent->GetMaterial(0) == LifeMaterial)
		MeshComponent->SetMaterial(0, GhostMaterialUnbreak);
}

void ASnakeElementBase::ReturnMaterial()
{
	if (MeshComponent->GetMaterial(0) == GhostMaterial)
		MeshComponent->SetMaterial(0, DefaultMaterial);
	if (MeshComponent->GetMaterial(0) == GhostMaterialUnbreak)
		MeshComponent->SetMaterial(0, LifeMaterial);
}

void ASnakeElementBase::HandleBeginOverlap(
	UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex,
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this, OtherActor, OtherComp);
	}
}

void ASnakeElementBase::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void ASnakeElementBase::SetToUnbreakble()
{
	IsUnbreakble = true;
	MeshComponent->SetMaterial(0, LifeMaterial);
}

void ASnakeElementBase::SetToBreakble()
{
	IsUnbreakble = false;
	MeshComponent->SetMaterial(0, DefaultMaterial);
}

bool ASnakeElementBase::GetIsUnbreakble()
{
	return IsUnbreakble;
}

// Called when the game starts or when spawned
void ASnakeElementBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASnakeElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementBase::Interact(AActor* Interactor, bool bIsHead, UPrimitiveComponent* OtherComp)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		if(Snake->GetInvulnerable() == false)
		Snake->ReduceLifeCount();
	}
	
}


