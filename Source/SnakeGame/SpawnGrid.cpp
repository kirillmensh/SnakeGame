// Fill out your copyright notice in the Description page of Project Settings.


#include "SpawnGrid.h"
#include "Food.h"
#include "Life.h"
#include "Invulnerability.h"
#include "SpeedBooster.h"

// Sets default values
ASpawnGrid::ASpawnGrid()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

void ASpawnGrid::RandomActorsSpawner()
{
	AFood* FoodObject = GetWorld()->SpawnActor<AFood>(FoodClass, RandomPointOnGrid());
	FoodObject->SpawnGridPtr = this;
	int RandSpawnNumber = FMath::RandRange(1, 3);
	switch (RandSpawnNumber)
	{
	case 1:
		GetWorld()->SpawnActor<ALife>(LifeClass, RandomPointOnGrid()); break;
	case 2:
		GetWorld()->SpawnActor<AInvulnerability>(InvulnerabilityClass, RandomPointOnGrid()); break;
	case 3:
		GetWorld()->SpawnActor<ASpeedBooster>(SpeedBoosterClass, RandomPointOnGrid()); break;
	default:
		break;
	}	
	
}

FTransform ASpawnGrid::RandomPointOnGrid()
{
	float RandX = FMath::RandRange(Xmin, Xmax);
	float RandY = FMath::RandRange(Ymin, Ymax);
	FTransform NewTransForm(FVector(RandY, RandX, Z + 50));
	return NewTransForm;
}

// Called when the game starts or when spawned
void ASpawnGrid::BeginPlay()
{
	Super::BeginPlay();
	UStaticMesh* Mesh = MeshComponent->GetStaticMesh();
	FBox box = Mesh->GetBounds().GetBox();
	float ScaleX = MeshComponent->GetComponentScale().X;
	float ScaleY = MeshComponent->GetComponentScale().Y;
	float Y = MeshComponent->GetComponentLocation().Y;
	float X = MeshComponent->GetComponentLocation().X;
	Z = MeshComponent->GetComponentLocation().Z;

	Xmin = ((box.Min.X * ScaleX)/1.3f - X);
	Xmax = ((box.Max.X * ScaleX)/1.3f - X);
	Ymin = ((box.Min.Y * ScaleY)/2 - Y);
	Ymax = ((box.Max.Y * ScaleY)/2 - Y);
	
	RandomActorsSpawner();
}

// Called every frame
void ASpawnGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

