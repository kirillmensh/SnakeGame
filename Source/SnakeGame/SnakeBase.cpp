// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interaction.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 10.f;
	MovementSpeed = 0.5f;
	LastMoveDirection = EMoveDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElements(5);
}

bool ASnakeBase::GetInvulnerable()
{
	return IsInvulnerable;
}

void ASnakeBase::DestroyAllElements()
{
	for (int i = SnakeElements.Num() - 1; i >= 0; i--)
	{
		SnakeElements[i]->DestroyElement();	
	}
	this->Destroy();
}

bool ASnakeBase::GetStepDone()
{
	return StepDone;
}

void ASnakeBase::AddLife()
{
	if(LifeCount < SnakeElements.Num())
	LifeCount++;
	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		auto CurrentElement = SnakeElements[i];
		if (CurrentElement->GetIsUnbreakble() == false)
		{			
			CurrentElement->SetToUnbreakble();
			break;
		}
	}
}

void ASnakeBase::ReduceLifeCount()
{
	LifeCount--;
	if (LifeCount < 1)
		DestroyAllElements();
	
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		if (CurrentElement->GetIsUnbreakble() == true)
		{
			CurrentElement->SetToBreakble();
			break;
		}
	}
}


int ASnakeBase::GetLifeCount()
{
	return LifeCount;
}

void ASnakeBase::SpeedIncrease(float SpeedIncriment)
{
	MovementSpeed -= SpeedIncriment;
	SetActorTickInterval(MovementSpeed);
}


void ASnakeBase::SetLastMovementDirection(EMoveDirection _LastMoveDirection)
{
	if (StepDone == true)
	{
		ASnakeBase::LastMoveDirection = _LastMoveDirection;
	}
	StepDone = false;
}

EMoveDirection ASnakeBase::GetLastMovementDirection()
{
	return LastMoveDirection;
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeBase::AddSnakeElements(int ElementNum)
{
	for (int i = 0; i < ElementNum; i++)
	{
		FVector NewLocation(0, 0, 0);
		if (SnakeElements.Num() > 0)
			NewLocation = SnakeElements.Last()->GetActorLocation();
		FTransform NewTransForm(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransForm);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();			
		}
		
	}
}

void ASnakeBase::Move()
{
	
	FVector MovementVector(ForceInitToZero);
	switch (LastMoveDirection)
	{
	case EMoveDirection::UP:
		MovementVector.X += ElementSize; 
		break;
	case EMoveDirection::DOWN:
		MovementVector.X -= ElementSize; 
		break;
	case EMoveDirection::RIGHT:
		MovementVector.Y += ElementSize; 
		break;
	case EMoveDirection::LEFT:
		MovementVector.Y -= ElementSize;
		break;
	}
	
	SnakeElements[0]->ToggleCollision();
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
	StepDone = true;

}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedBlock, AActor* OtherActor, UPrimitiveComponent* OtherComp)
{
	if (IsValid(OverlappedBlock))
	{
		int32 ElementIndex;
		SnakeElements.Find(OverlappedBlock, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteraction* InteractionInterface = Cast<IInteraction>(OtherActor);
		if (InteractionInterface)
		{
			InteractionInterface->Interact(this, bIsFirst, OtherComp);
		}
	}
}

void ASnakeBase::SetToInvulnerable()
{
	IsInvulnerable = true;
	GetWorldTimerManager().SetTimer(MemberTimerHandle, this, &ASnakeBase::InvulnerabilityTimer, 5.0f, false);

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		CurrentElement->ChangeMaterial();
	}
}

void ASnakeBase::InvulnerabilityTimer()
{
	IsInvulnerable = false;
	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		CurrentElement->ReturnMaterial();
	}
}