// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnGrid.generated.h"

class ASpeedBooster;
class AFood;
class ALife;
class AInvulnerability;

UCLASS()
class SNAKEGAME_API ASpawnGrid : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnGrid();
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASpeedBooster> SpeedBoosterClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ALife> LifeClass;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AInvulnerability> InvulnerabilityClass;

	void RandomActorsSpawner();	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	FTransform RandomPointOnGrid();
	float Xmin = 0.f;
	float Xmax = 0.f;
	float Ymin = 0.f;
	float Ymax = 0.f;
	float Z = 0.f;

};
