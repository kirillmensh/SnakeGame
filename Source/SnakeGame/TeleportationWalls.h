// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interaction.h"
#include "TeleportationWalls.generated.h"


UCLASS()
class SNAKEGAME_API ATeleportationWalls : public AActor, public IInteraction
{
	GENERATED_BODY()
	
	
public:	
	// Sets default values for this actor's properties
	ATeleportationWalls();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		USceneComponent* Root;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* WallRight;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* WallLeft;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* WallUp;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* WallDown;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interact, bool bIsHead, UPrimitiveComponent* OtherComp) override;

};
