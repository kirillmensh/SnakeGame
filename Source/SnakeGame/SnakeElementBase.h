// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interaction.h"
#include "SnakeElementBase.generated.h"

class UStaticMeshComponent;
class ASnakeBase;
class ATeleportationWalls;

UCLASS()
class SNAKEGAME_API ASnakeElementBase : public AActor, public IInteraction
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeElementBase();
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* LifeMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* DefaultMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* GhostMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* GhostMaterialUnbreak;

	UFUNCTION(BlueprintNativeEvent)
	void SetFirstElementType();
	void SetFirstElementType_Implementation();
    
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void Interact(AActor* Interactor, bool bIsHead, UPrimitiveComponent* OtherComp) override;
	UPROPERTY()
		ASnakeBase* SnakeOwner;
	UFUNCTION()
		void HandleBeginOverlap(
			UPrimitiveComponent* OverlappedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);
	UFUNCTION()
		void ToggleCollision();
	void SetToUnbreakble();
	void SetToBreakble();
	bool GetIsUnbreakble();
	void DestroyElement();
	void ChangeMaterial();
	void ReturnMaterial();
private:
	bool IsUnbreakble = false;
};
